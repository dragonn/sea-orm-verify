# sea-orm-verify

Provides `Verify` derive macro.

```rust, ignore
#[derive(DeriveEntityModel, Verify)]
#[derive(Debug, Clone, PartialEq)]
#[sea_orm(table_name = "task")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: u32,
    pub finish_at: Option<DateTime>,
}
```

generates

```rust, ignore
impl Model {
    async fn _verify() {
        sqlx::query_as!(Self, "SELECT id, finish_at FROM task");
    }
}
```

this will cause sqlx query_as macro to verify the struct fields with the database at compile time
needs to have setup DATABASE_URL for example using .env or sqlx offline data. Please refer to [docs.rs/sqlx](https://docs.rs/sqlx/latest/sqlx/macro.query_as.html)

it also needs sqlx as a dependency in your project, for example for Postgres in Cargo.toml it needs:
```toml
sqlx = { version = "0.6", features = ["runtime-tokio-rustls", "postgres"] }
```

Please be aware that sqlx and sea-orm column mapping might be not 100% compatible and this check might fail to catch some column type mismatch you still have

Supported struct attributes inside `#[sea_orm()`:
* `table_name` - gets the database table name
* `schema_name` - gets the database schema name (for Postgres)

Supported field attributes inside `#[verify()]`:
* `type_override` - override sqlx type like described in [Force a Different/Custom Type](https://docs.rs/sqlx/latest/sqlx/macro.query.html#force-a-differentcustom-type), useful for custom enums. Enum need to annotated with `#[derive(sqlx::Type)` and `#[sqlx(type_name = "integer")]`
* `not_null` - forces the column to be NOT NULL, useful for example for db views where sqlx get the nullable wrong.
* `null` - same as `not_null` but forces the column to be NULL